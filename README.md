# @softcripto/utils

Collection of utility libraries

## Install

```bash
npm install @softcripto/utils --save
```

## Usage

There are several utility methods. 

### Dependency Injector

**Why?** Submodule should not use any services by directly requiring from other submodules but we have
**dependency injector** to use external services



```javascript
const dependencies = require("./dependencies"); //this is target object where external services will be injected
const injectDependencies = require("@softcripto/utils/injectDependencies"); 

injectDependencies(dependencies, externals, __filename); //
```

## License

[MIT](http://vjpr.mit-license.org)