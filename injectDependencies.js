'use strict';

/**
 * Injects Dependencies
 * Aborts process if expected dependencies (defined in $inject) are missing
 * 
 * @param {Object} target - Target Object who holds dependencies
 * @param {Array.<String>} target.$inject - properties to examine
 * @param {Object} dependencies - Properties to copy from
 * @param {String} sourceFile - file path that invoked function
 * 
 */
module.exports = function (target, dependencies, sourceFile) {
    Object.assign(target, dependencies);
    //validate required dependencies
    target.$inject.forEach((key) => {
        if (!dependencies.hasOwnProperty(key)) {
            console.log(`Error from file "${sourceFile}" \n
            Dependency "${key}" is missing \n
            `);
            process.abort();
        }
    });
};