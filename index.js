'use strict';
const injectDependencies = require("./injectDependencies"); 
/**
 * @module
 */
module.exports = {
    injectDependencies
};
